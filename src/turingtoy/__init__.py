from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:

    tape = list(
        input_
    )  # Convertit l'input en une liste pour représenter la bande de la machine de Turing
    current_state = machine["start state"]  # État courant initialisé à l'état de départ
    current_position = 0  # Position courante de la tête de lecture/écriture

    execution_order = []  # Liste pour stocker l'ordre d'exécution des états
    execution_order.append(
        current_state
    )  # Ajoute l'état de départ à l'ordre d'exécution

    while steps is None or steps > 0:
        if current_state in machine["final states"]:
            return (
                "".join(tape),
                execution_order,
                True,
            )  # Si l'état courant est un état final, retourne le résultat

        if current_position < 0:
            tape.insert(
                0, machine["blank"]
            )  # Insère un symbole vierge (blank) si la tête de lecture/écriture sort de la bande à gauche
            current_position = 0
        elif current_position >= len(tape):
            tape.append(
                machine["blank"]
            )  # Ajoute un symbole vierge (blank) si la tête de lecture/écriture sort de la bande à droite

        current_symbol = tape[
            current_position
        ]  # Symbole sous la tête de lecture/écriture
        transition = machine["table"][current_state].get(
            current_symbol
        )  # Transition correspondant à l'état courant et au symbole courant

        if transition is None:
            return (
                "".join(tape),
                execution_order,
                False,
            )  # Si aucune transition n'est définie, retourne le résultat avec False

        tape[current_position] = transition.get(
            "write", current_symbol
        )  # Écrit le nouveau symbole sur la bande

        if transition.get("L"):
            current_position -= 1  # Déplace la tête de lecture/écriture vers la gauche
        elif transition.get("R"):
            current_position += 1  # Déplace la tête de lecture/écriture vers la droite

        current_state = (
            transition.get("L") or transition.get("R") or transition.get("write")
        )  # Met à jour l'état courant

        execution_order.append(
            current_state
        )  # Ajoute l'état courant à l'ordre d'exécution

        if steps is not None:
            steps -= 1

    return (
        "".join(tape),
        execution_order,
        False,
    )  # Si le nombre maximum d'étapes est atteint, retourne le résultat avec False